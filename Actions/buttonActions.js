export const toggleStartButton = () => {
  return {
    type: "TOGGLE_START_BUTTON",
  };
};

export const toggleStopButton = () => {
  return {
    type: "TOGGLE_STOP_BUTTON",
  };
};

export const toggleResetButton = () => {
  return {
    type: "TOGGLE_RESET_BUTTON",
  };
};

export const toggleLapButton = () => {
  return {
    type: "TOGGLE_LAP_BUTTON",
  };
};
