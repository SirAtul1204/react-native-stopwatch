export const startTimer = timer => {
  return {
    type: "START_TIMER",
    payload: {
      timer,
    },
  };
};

export const stopTimer = () => {
  return {
    type: "STOP_TIMER",
  };
};

export const resetTimer = () => {
  return {
    type: "RESET_TIMER",
  };
};

export const createLap = () => {
  return {
    type: "CREATE_LAP",
  };
};
