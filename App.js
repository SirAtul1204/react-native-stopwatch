import React from "react";
import {Text, StyleSheet, SafeAreaView, View} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {
  toggleLapButton,
  toggleResetButton,
  toggleStartButton,
  toggleStopButton,
} from "./Actions/buttonActions";
import {
  createLap,
  resetTimer,
  startTimer,
  stopTimer,
} from "./Actions/watchActions";
import Btn from "./Components/Btn";
import LapTable from "./Components/LapTable";
import Watch from "./Components/Watch";

const styles = StyleSheet.create({
  body: {
    display: "flex",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  major: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flex: 1.7,
  },

  btnContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: 200,
    marginTop: 20,
  },
});

const App = () => {
  const dispatch = useDispatch();
  const {
    isStartButtonDisabled,
    isStopButtonDisabled,
    isResetButtonDisabled,
    isLapButtonDisabled,
  } = useSelector(state => state.button);

  const handleStart = () => {
    const timer = setInterval(() => {
      dispatch(startTimer(timer));
    }, 10);

    dispatch(toggleStartButton());
    dispatch(toggleStopButton());
    dispatch(toggleResetButton());
    dispatch(toggleLapButton());
  };

  const handleStop = () => {
    dispatch(stopTimer());
    dispatch(toggleStopButton());
    dispatch(toggleStartButton());
    dispatch(toggleResetButton());
    dispatch(toggleLapButton());
  };

  const handleReset = () => {
    dispatch(resetTimer());
  };

  const handleLap = () => {
    dispatch(createLap());
  };

  return (
    <SafeAreaView style={styles.body}>
      <View style={styles.major}>
        <Watch />
        <View style={styles.btnContainer}>
          <Btn
            title={"Start"}
            backColor={"#00ff00"}
            pressHandler={handleStart}
            isDisabled={isStartButtonDisabled}
          />
          <Btn
            title={"Stop"}
            backColor={"#ffff00"}
            pressHandler={handleStop}
            isDisabled={isStopButtonDisabled}
          />
        </View>
        <View style={styles.btnContainer}>
          <Btn
            title={"Reset"}
            backColor={"#ff0000"}
            pressHandler={handleReset}
            isDisabled={isResetButtonDisabled}
          />
          <Btn
            title={"Lap"}
            backColor={"#00ffff"}
            pressHandler={handleLap}
            isDisabled={isLapButtonDisabled}
          />
        </View>
      </View>
      <LapTable />
    </SafeAreaView>
  );
};

export default App;
