import React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {useSelector} from "react-redux";

const styles = StyleSheet.create({
  btn: {
    paddingVertical: 10,
    borderRadius: 5,
    shadowColor: "#171717",
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 10,
    borderColor: "#000",
    borderWidth: 2,
    width: 80,
    display: "flex",
    alignItems: "center",
  },

  text: {
    color: "#000",
    fontFamily: "Fira Code",
    fontSize: 15,
  },
});

const Btn = ({title, backColor, pressHandler, isDisabled}) => {
  return (
    <TouchableOpacity
      style={{
        ...styles.btn,
        backgroundColor: isDisabled ? "#808080" : backColor,
      }}
      onPress={pressHandler}
      disabled={isDisabled}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Btn;
