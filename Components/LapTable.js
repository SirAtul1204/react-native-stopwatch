import React from "react";
import {ScrollView, Text, StyleSheet} from "react-native";
import {useSelector} from "react-redux";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    textAlign: "center",
    fontSize: 33,
    color: "#0033ff",
  },
  text: {
    fontSize: 30,
    color: "#000",
  },
});

const LapTable = () => {
  const {laps} = useSelector(state => state.watch);
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.heading}>Laps</Text>
      {laps.map((lap, index) => {
        return (
          <Text
            key={
              String(lap[0] < 10 ? "0" + String(lap[0]) : lap[0]) +
              ":" +
              String(lap[1] < 10 ? "0" + String(lap[1]) : lap[1]) +
              ":" +
              String(lap[2] < 10 ? "0" + String(lap[2]) : lap[2])
            }
            style={styles.text}>
            {index + 1}. {lap[0] < 10 ? "0" + String(lap[0]) : lap[0]} :{" "}
            {lap[1] < 10 ? "0" + String(lap[1]) : lap[1]} :{" "}
            {lap[2] < 10 ? "0" + lap[2] : lap[2]}
          </Text>
        );
      })}
    </ScrollView>
  );
};

export default LapTable;
