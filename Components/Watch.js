import React from "react";
import {Text, StyleSheet} from "react-native";
import {useSelector} from "react-redux";

const styles = StyleSheet.create({
  watch: {
    fontFamily: "Fira Code",
    fontWeight: "600",
    fontSize: 45,
    color: "#000",
  },
});

const Watch = () => {
  const {minutes, seconds, centiseconds} = useSelector(state => state.watch);
  return (
    <Text style={styles.watch}>
      {minutes < 10 ? "0" + String(minutes) : minutes} :{" "}
      {seconds < 10 ? "0" + String(seconds) : seconds} :{" "}
      {centiseconds < 10 ? "0" + String(centiseconds) : centiseconds}
    </Text>
  );
};

export default Watch;
