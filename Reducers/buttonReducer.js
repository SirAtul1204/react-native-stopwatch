const initialState = {
  isStartButtonDisabled: false,
  isStopButtonDisabled: true,
  isResetButtonDisabled: false,
  isLapButtonDisabled: true,
};

export const buttonReducer = (state = initialState, action) => {
  let newState = {...state};
  switch (action.type) {
    case "TOGGLE_START_BUTTON":
      newState.isStartButtonDisabled = !newState.isStartButtonDisabled;
      break;
    case "TOGGLE_STOP_BUTTON":
      newState.isStopButtonDisabled = !newState.isStopButtonDisabled;
      break;
    case "TOGGLE_RESET_BUTTON":
      newState.isResetButtonDisabled = !newState.isResetButtonDisabled;
      break;
    case "TOGGLE_LAP_BUTTON":
      newState.isLapButtonDisabled = !newState.isLapButtonDisabled;
      break;
    default:
      break;
  }
  return newState;
};
