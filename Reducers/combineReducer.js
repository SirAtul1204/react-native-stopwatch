import {combineReducers} from 'redux';
import {watchReducer} from './watchReducer';
import {buttonReducer} from './buttonReducer';
export const rootReducer = combineReducers({
  watch: watchReducer,
  button: buttonReducer,
});
