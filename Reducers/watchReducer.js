const initialState = {
  minutes: 0,
  seconds: 0,
  centiseconds: 0,
  timer: null,
  laps: [],
};

export const watchReducer = (state = initialState, action) => {
  let newState = {...state};
  switch (action.type) {
    case "START_TIMER":
      newState.timer = action.payload.timer;
      newState.centiseconds++;
      if (newState.centiseconds > 99) {
        newState.centiseconds = 0;
        newState.seconds++;
        if (newState.seconds > 59) {
          newState.minutes++;
        }
      }
      break;
    case "STOP_TIMER":
      clearInterval(newState.timer);
      break;
    case "RESET_TIMER":
      newState = initialState;
      newState.laps = [];
      break;
    case "CREATE_LAP":
      newState.laps.push([
        newState.minutes,
        newState.seconds,
        newState.centiseconds,
      ]);
      break;
    default:
      break;
  }
  return newState;
};
